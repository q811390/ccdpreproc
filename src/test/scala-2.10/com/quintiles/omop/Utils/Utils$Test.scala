package com.quintiles.omop.Utils

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{Path, FileSystem}
import org.scalatest.FunSuite

/**
 * Created by cloudera on 6/9/15.
 */
class Utils$Test extends FunSuite {

  test("testCopyFromHadoopToLocal") {
    val fc = FileCopy(sourceFs = "hdfs://127.0.0.1:8020", sourcePath = "/user/cloudera/result1/zips1_result_seq", destFs = "hdfs://127.0.0.1:8020",destPath = "/user/cloudera/result/zips1_result_seq",temp = "/tmp",fileName = "zips1_result_seq")
    FSUtils.moveFileFromHadoopToHadoop(fc)

    val conf = new Configuration()
    conf.set("fs.defaultFS", fc.destFs)
    val fs = FileSystem.get(conf)
    //val destPath: String = fc.destPath + "/" + fc.fileName
    println(" check if file exists "+fc.destPath)
    assert(fs.exists(new Path(fc.destPath)))

  }

  test("testMoveFileFromHadoopToHadoop") {

  }

}
