package com.quintiles.omop.Utils

import java.io.{File, IOException}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{Path, FileSystem}

import scala.collection.mutable.{ArrayBuffer, ListBuffer}

/**
 * Created by cloudera on 6/9/15.
 */


/**
 *
 * @param sourceFs fs.defaultFS path for source hadoop cluster
 * @param sourcePath path to the source file in hadoop fs
 * @param destFs  fs.defaultFS path for dest hadoop cluster
 * @param destPath  path to the dest file in hadoop fs
 * @param temp temp path where the file can be copied. Also needs to have write and read permission.
 * @param fileName the name of the file.
 */
case class FileCopy(sourceFs:String, sourcePath:String, destFs:String, destPath:String, temp:String, fileName:String)

object FSUtils {


  def main(args: Array[String]) {
//    val vc = FileCopy("hdfs://127.0.0.1:8020",
//      "/user/cloudera/result1/member_metadata.parquet",
//      "hdfs://127.0.0.1:8020",
//      "/user/cloudera/result/inputRootZips/metadata/member/member_metadata.parquet",
//      "/tmp/","/member_metadata.parquet")
//    moveFileFromHadoopToHadoop(vc)

    println(getFolderPaths("/home/cloudera/Desktop/delme/",false).toList)

  }

  def moveFileFromHadoopToHadoop(fc:FileCopy):Unit = {

    copyFromHadoopToLocal(fc)
    val nfc = fc.copy(temp = fc.temp +"/"+ fc.fileName )//.copy(destPath = fc.destPath + "/" + fc.fileName)
    copyFromLocalToHadoop(nfc)
  }



  def copyFromLocalToHadoop(fc:FileCopy):Unit = {

       val conf = new Configuration()
      conf.set("fs.defaultFS", fc.destFs)
      val fs = FileSystem.get(conf)
      fs.moveFromLocalFile(new Path(fc.temp), new Path(fc.destPath))
      println("source "+fc.temp+" dest  "+fc.destPath)


  }

  def copyFromHadoopToLocal(fc:FileCopy):Unit = {

    val conf = new Configuration()
    conf.set("fs.defaultFS", fc.sourceFs)
    val fs = FileSystem.get(conf)
    fs.moveToLocalFile( new Path(fc.sourcePath), new Path(fc.temp))


  }

  /**
   * Takes the root directory and returns the directories with in it.
   * @param rooltFolder
   * @return
   */
  def getFolderPaths(rooltFolder: String, folders: Boolean): List[String] = {
    var listBuffer: ListBuffer[String] = new ListBuffer[String]()
    val folder = new File(rooltFolder)
    for (fileEntry <- folder.listFiles) {
      if (folders && fileEntry.isDirectory) listBuffer += fileEntry.getAbsolutePath.toString
      if (!folders && fileEntry.isFile) listBuffer += fileEntry.getAbsolutePath.toString
    }
    listBuffer.toList
  }

  /**
   * Return the zip files in the directory recursively.
   * @param folder
   * @return
   */
  def listFilesForFolder(folder: File): Array[String] = {
    val files  = new ArrayBuffer[String]
    for (fileEntry <- folder.listFiles) {
      if (fileEntry.isDirectory) {
        listFilesForFolder(fileEntry)
      }
      else if (fileEntry.isFile && fileEntry.getName.endsWith(".zip")) {
        files += (fileEntry.getAbsolutePath.toString)
      }
    }
    return files.toArray
  }


}
