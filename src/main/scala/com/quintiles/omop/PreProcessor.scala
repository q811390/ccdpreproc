package com.quintiles.omop

/**
 * Created by Dhiraj Peechara on 6/3/15.
 * TODO :
 * 1.  Standardize FileCopy.
 * 2.  Use DataFrame instead of RDD
 * 3.
 */


import java.io.{BufferedReader, File, FileReader}
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, Paths}
import java.util.Properties
import java.util.concurrent.{TimeUnit, Executors, ExecutorService, Callable}
import com.q.winzipeas.xml.metadata.{XmlMetaData, MetaData}
import com.quintiles.hadoop.util.{HadoopFileUtil, TarToSeqFile}
import com.quintiles.omop.Utils.{FileCopy, FSUtils}
import com.twitter.util.FutureTask
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, FileUtil}
import org.apache.hadoop.io.compress.{BZip2Codec, GzipCodec}
import org.apache.spark.SparkContext._
import com.avor.Unzip
import org.apache.hadoop.io.{BytesWritable, Text}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import parquet.hadoop.ParquetOutputFormat
import scala.collection
import scala.collection.immutable.IndexedSeq
import scala.collection.mutable.ListBuffer
import org.apache.spark.sql._

/**
 * Created by q811390 on 5/29/2015.
 */

case class XmlKeyValue(fileName: String, fileContent: String)

case class MetaDataSer(fileName: String, patient_id: String, document_id: String, check_sum: String, member_id: String, xmlByteLength: String, duplicates: Boolean = false, errorCode: Int = 0, errorMsg: String = null)

case class MemberMetadata(member_Id: String, input_zips: Int, output_zips: Int, sequence_file_name: String, zip_file_names: Array[String])

case class ProcessFolderArgs(folderPath: String, sc: SparkContext, sqlContext: SQLContext, tempPath: String, srcPath: String, srctNameNode: String, distNameNode: String, destPath: String, numPartitions: Int)

object PreProcessor {

  val validateAvro: Boolean = false

  var poolSize: Int = 5

  //def writeSuccessFile(content: Map[String,String], s: String):Unit = {}

  def main(args: Array[String]) {

    //val pool: ExecutorService = Executors.newFixedThreadPool(poolSize)

    val props = loadProperties(args)
    val numPartitions = if (args.length > 2) args(2).toInt else 12

    val PP_CURRENT_RUN_DATE = "pp.current.run.date"
    val PP_HDFS_LOC = "pp.hdfs.loc"
    val PP_ENV = "pp.env"
    val PP_INPUT_PATH = "pp.input.path"
    val inputPathDir = props.getProperty(PP_INPUT_PATH)
    val currentRunDate = props.getProperty(PP_CURRENT_RUN_DATE)
    // The root folder which should contain sub folders and with in the sub folders will be zip files.
    val inputRootFolder: String = if (inputPathDir == null) props.getProperty("INPUT_ROOT_FOLDER") else (props.getProperty(PP_INPUT_PATH) + currentRunDate)
    val env = props.getProperty(PP_ENV)
    val hdfsLoc = props.getProperty(PP_HDFS_LOC)

    val tempStoragePath: String = props.getProperty("TEMP_STORAGE_PATH")
    val srcPath: String = props.getProperty("HDFS_BASE_DIR")
    val srcNameNode = props.getProperty("SRC_DEFAULT_FS")
    val distNameNode = props.getProperty("DIST_DEFAULT_FS")
    val filterProviders = props.getProperty("PROCESS_DIRS")
    //var tempSplitFiles:Seq[String] = null

    val splitFiles = if (filterProviders != null) {
      val tempFilterProvider = filterProviders.substring(7,filterProviders.length)
      if (Files.isDirectory(Paths.get(tempFilterProvider)))
        FSUtils.getFolderPaths(tempFilterProvider, false)
      else
        Seq(filterProviders)
    } else
       Seq()


    println("split files are ::: "+splitFiles.toList+"  and tempSplitfiles are  "+filterProviders.endsWith("*")+" filterProviders "+filterProviders)

    //def processSplitfile(splitfile:String):Unit{""}



    val conf = if (args.length == 0) {
      new SparkConf()
        .setMaster("local[3]")
        .setAppName("Omop_Pre")
        .set("spark.driver.memory", "13g")

    } else {
      new SparkConf()
    }
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)



    //val folders: List[String] = FSUtils.getFolderPaths(inputRootFolder, true)
    //println(" totoal number of folder in the root folder are ::  " + folders.length)


    import scala.collection.JavaConversions._
    splitFiles.foreach(splitFile => {

      println(" working on split file :: "+splitFile)

      val providers = if (splitFile != null) {
        val filterProdiersCol = sc.textFile("file://"+splitFile).collect()
        filterProdiersCol
      }
      else null


      val distPath = (if (hdfsLoc == null) props.getProperty("HDFS_DIST_PATH") else hdfsLoc) + (if (filterProviders != null) Paths.get(splitFile).getFileName.toString else currentRunDate)

      var memberMetdata: Seq[MemberMetadata] = null

      if (providers != null) {
        //val folderStrings: Array[String] = providers //folders.filter(a => providers.exists(a.endsWith))
        memberMetdata = providers.map(a => processMemberFolder(ProcessFolderArgs(a, sc, sqlContext, tempStoragePath, srcPath, srcNameNode, distNameNode, distPath, numPartitions), splitFile, sqlContext))
      }
      //      else {
      //        memberMetdata = folders.map(a => processMemberFolder(ProcessFolderArgs(a, sc, sqlContext, tempStoragePath, srcPath, srcNameNode, distNameNode, distPath, numPartitions), filterProviders, sqlContext))
      //      }

      val resultFileName = if (filterProviders != null) Paths.get(splitFile).getFileName.toString else currentRunDate

      // write success file

      println("writing member metadata")
      // writing member metadata file
      import sqlContext.implicits._

      val memberMetaDataSrcPath: String = srcPath +"/"+Paths.get(splitFile).getFileName.toString +"/member_metadata.parquet"


//      val rdds = memberMetdata.map(a => sc.parallelize(Seq(a)))
//      val colRdd = rdds.reduce(_++_)
//      colRdd.toDF.saveAsParquetFile(memberMetaDataSrcPath)
      //srcPath, srcNameNode, distNameNode, distPath

//      val fc = FileCopy(sourceFs = srcNameNode, sourcePath = memberMetaDataSrcPath, destFs = distNameNode, destPath = distPath + "/metadata/member/member_metadata.parquet", temp = "/tmp/", fileName = "member_metadata.parquet")
//      println(fc)
//      FSUtils.moveFileFromHadoopToHadoop(fc)


      val successContent = Map(PP_CURRENT_RUN_DATE -> resultFileName, PP_ENV -> env, PP_HDFS_LOC -> distPath)
      println("writing the success file...")
      HadoopFileUtil.writeHadoopFile(successContent, hdfsLoc, distNameNode)

    })


    println("Exiting the program......Good bye")
  }


  /**
   * Takes in input folder and writes the sequence file to the destination hdfs.
   * @param memberFolder
   */
  def processMemberFolder(memberFolder: ProcessFolderArgs, filterProviders: String, sqlContext: SQLContext): MemberMetadata = {
    import org.apache.spark.rdd.RDD
    import org.apache.spark.{SparkConf, SparkContext}
    import org.apache.spark.sql.SQLContext
    import sqlContext.implicits._
    println("processing member folder " + memberFolder.folderPath)
    val tempSeqPath = memberFolder.srcPath + Paths.get(memberFolder.folderPath).getFileName.toString + "_input_seq"
    val folderName = Paths.get(memberFolder.folderPath).getFileName.toString
    val resultFileName: String = (if (filterProviders != null) Paths.get(filterProviders).getFileName.toString else Paths.get(memberFolder.folderPath).getFileName.toString) + "_result_seq"
    val resultSeqPath = memberFolder.srcPath + resultFileName
    val metaDataFileName: String = Paths.get(memberFolder.folderPath).getFileName.toString + "_metadata_parquet"
    val metaDataSeqPath = memberFolder.srcPath +"/"+ Paths.get(filterProviders).getFileName.toString +"/"+ metaDataFileName


    import scala.collection.JavaConversions._
    val filesList: IndexedSeq[String] = new TarToSeqFile().listFilesForFolder(new File(memberFolder.folderPath)).toIndexedSeq
    val pertitionCount = filesList.size / memberFolder.numPartitions match {
      case x if 0 until 100 contains x => memberFolder.numPartitions
      case x if 100 until 200 contains x => memberFolder.numPartitions * 2
      case x if 200 until 300 contains x => memberFolder.numPartitions * 3
      case x if 300 until 400 contains x => memberFolder.numPartitions * 4
      case x if 400 until 500 contains x => memberFolder.numPartitions * 5
      case x if 500 until 600 contains x => memberFolder.numPartitions * 6
      case x if 600 until 700 contains x => memberFolder.numPartitions * 7
      case x if 700 until 800 contains x => memberFolder.numPartitions * 8
      case x if 800 until 900 contains x => memberFolder.numPartitions * 9
      case _ => memberFolder.numPartitions * 10
    }
    val filesRdd: RDD[String] = memberFolder.sc.parallelize(filesList, pertitionCount).cache()

    val goodFilesCounter = memberFolder.sc.accumulator(0)
    val badFilesCounter = memberFolder.sc.accumulator(0)

    import sqlContext.implicits._
    val bunzipped: RDD[(Tuple2[String, String], MetaDataSer)] = filesRdd.map(a => {

      val path: Path = Paths.get(a)
      var m: (String, String) = null
      try {
        m = (path.getFileName.toString + ".xml", Unzip.getFileBytes(new File(path.toAbsolutePath.toString), "cXVpbnRpbGVzIHN0cm9uZyBwYXNzd29yZA=="))
      } catch {
        case e: Exception => m = (path.getFileName.toString, null)
      }

      val mm = if (m._2 != null) {

        var mser: MetaDataSer = null
        try {
          val metaData = XmlMetaData.getxmlMetadata(m._2.getBytes)
          mser = MetaDataSer(path.getFileName.toString, metaData.getPatient_id, metaData.getDocument_id, metaData.getCheck_sum, metaData.getMember_id, metaData.getXmlByteLength)
          goodFilesCounter += 1
          (m, mser)
        } catch {
          case e: Exception => m = (path.getFileName.toString, null)
        }
        (m, mser)
      }
      else {
        badFilesCounter += 1
        (m, null)

      } //Files.delete(path)
      mm
    }).cache()



    val goodfilesRdd = bunzipped.filter(a => a._2 != null)
    val badFilesRdd = bunzipped.filter(a => a._2 == null)

    def removeDuplicates(): (RDD[((String, String), MetaDataSer)], collection.Map[String, Int]) = {

      val duplicates: collection.Map[String, Int] = goodfilesRdd.map(a => (a._2.patient_id)).map(a => (a, 1)).reduceByKey((a, b) => a + b).filter(a => a._2 > 1).collectAsMap() // inefficient. Need to imrove  (cannot scale).
      val filteredUnzipped = goodfilesRdd.filter(a => !(duplicates.contains(a._2.patient_id)))

      (filteredUnzipped.cache(), duplicates)
    }
    import org.apache.spark.rdd._
    val removedDups: (RDD[((String, String), MetaDataSer)], collection.Map[String, Int]) = removeDuplicates
    val listDupPatients = removedDups._2
    val countDips = listDupPatients.foldLeft(0) { (a, b) => a + b._2 }
    val actualGoodXMLcount = goodFilesCounter.value - countDips
    val badFiles = bunzipped.filter(a => a._1._2 == null).collect()
    println("printing bad files.....")
    badFiles.foreach(b => println("bad file #################" + b._1._1))


    val finalResultSeqPath = folderName+"_"+filesList.length + "__" + actualGoodXMLcount + "__" + resultFileName
    removedDups._1.keys.saveAsSequenceFile(resultSeqPath, Some(classOf[BZip2Codec]))

    val df = removedDups._1.values.map(a => ({
      if (listDupPatients.contains(a.patient_id)) a.copy(duplicates = true) else a
    })).toDF()
    df.saveAsParquetFile(metaDataSeqPath)


    val memberMetaData = MemberMetadata(member_Id = Paths.get(memberFolder.folderPath).getFileName.toString, filesList.length, actualGoodXMLcount, resultFileName, filesList.toArray)

    val fc = FileCopy(sourceFs = memberFolder.srctNameNode, sourcePath = resultSeqPath, destFs = memberFolder.distNameNode, destPath = memberFolder.destPath + "/seqfiles/" + finalResultSeqPath, temp = "/tmp", fileName = resultFileName)
    println(fc)
    FSUtils.moveFileFromHadoopToHadoop(fc)

//    val fcMeta = FileCopy(sourceFs = memberFolder.srctNameNode, sourcePath = metaDataSeqPath, destFs = memberFolder.distNameNode, destPath = memberFolder.destPath + "/metadata/patients/" + metaDataFileName, temp = "/tmp", fileName = metaDataFileName)
//    println(fcMeta)
//    FSUtils.moveFileFromHadoopToHadoop(fcMeta)
    removedDups._1.unpersist(false)
    bunzipped.unpersist(false)
    filesRdd.unpersist(false)
    memberMetaData

  }

  def loadProperties(filePath: Array[String]): Properties = {
    val t = new Properties()
    t.load(new BufferedReader(new FileReader(filePath(0))))
    if (filePath.length > 1) {
      val p = new Properties()
      p.load(new BufferedReader(new FileReader(filePath(1))))
      t.putAll(p)
    }
    t
  }

  /**
   * Copy the locally created sequence file to hdfs and delete the sequence file on local file system.
   * @param sourcePath
   * @param distPath
   * @param nameNode
   */
  def copyFileToHDFS(sourcePath: String, distPath: String, nameNode: String): Unit = {
    println(s" copy file from ${sourcePath}  to ${distPath}")
    val conf: Configuration = new Configuration()
    conf.set("fs.defaultFS", nameNode)
    val hdfsFileSystem: FileSystem = FileSystem.get(conf)

    val local = new org.apache.hadoop.fs.Path(sourcePath)
    val hdfs = new org.apache.hadoop.fs.Path(distPath)

    hdfsFileSystem.copyFromLocalFile(local, hdfs)
    println("File " + local + " copied to local machine on location: " + hdfs)

    Files.delete(Paths.get(sourcePath))

  }


}



