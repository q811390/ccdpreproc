package com.avor;


/**
 * Created by q811390 on 5/29/2015.
 */
        import java.io.BufferedWriter;
        import java.io.ByteArrayOutputStream;
        import java.io.File;
        import java.io.FileOutputStream;
        import java.io.FileWriter;
        import java.io.IOException;
        import java.io.PrintWriter;
        import java.util.regex.Pattern;
        import java.util.zip.DataFormatException;

        import org.apache.commons.io.FileUtils;
        import org.apache.commons.io.FilenameUtils;

//import com.q.winzipeas.xml.duplicatecheck.XMLPatientDulicateChecker;
//import com.q.winzipeas.xml.metadata.MetaData;
//import com.q.winzipeas.xml.metadata.XmlMetaData;

        import de.idyl.winzipaes.AesZipFileDecrypter;
        import de.idyl.winzipaes.impl.AESDecrypterBC;

public class Unzip {

    // public XMLPatientDulicateChecker duplicatePatientChecker =null;
    public Unzip() {

    }



    public static String getFileBytes(File theCompressedFile, String thePassword) throws Exception{
        AesZipFileDecrypter dec = null;
        // long startTime = System.currentTimeMillis();
        try {


            dec = new AesZipFileDecrypter(theCompressedFile,
                    new AESDecrypterBC());

            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            if (dec.getNumberOfEntries() == 0) {
                dec.close();
            }

            dec.extractEntry(dec.getEntryList().get(0), outStream, thePassword);

            return new String(outStream.toByteArray());
        } finally {
            if (dec != null) {
                dec.close();
            }
            //   long endTime = System.currentTimeMillis();
        }
    }

    /**
     * Optional variables, metaDataOutPutDir and pastMetaDataOuputDir
     * @param theCompressedFile
     * @param thePassword
     * @param theOutputDir
     * @throws IOException
     * @throws DataFormatException
     */
    public static void getDecryptedzipData(File theCompressedFile, String thePassword,	String theOutputDir) throws Exception{
        AesZipFileDecrypter dec = null;
        long startTime = System.currentTimeMillis();
        try {


            dec = new AesZipFileDecrypter(theCompressedFile,
                    new AESDecrypterBC());

            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            if (dec.getNumberOfEntries() == 0) {
                dec.close();
            }

            dec.extractEntry(dec.getEntryList().get(0), outStream, thePassword);
            //System.out.println(theOutputDir+ FilenameUtils.removeExtension(theCompressedFile.getName()) + ".xml");
            String xmlOutputFilePath=theOutputDir+ FilenameUtils.removeExtension(theCompressedFile.getName()) + ".xml";
            File xmlFile = new File(xmlOutputFilePath);

			/*
			 * Gather Meta data about the xml file we will use this to figure out if the file is new or existing with delta or no change
			 * If the file is new put it in the new bucket and record meta data
			 * if the file is existing with delta put it in the delta bucket and record meta data
			 * if the file is existing with no delta, do nothing, yet record the meta data.
			 */
            // MetaData metaData = XmlMetaData.getMetaData(member_id,xmlOutputFilePath,pastMetaDataOutputDir,outStream.toByteArray());

            long zipUncompressedSize = dec.getEntryList().get(0).getSize();

            // //create file in new dir with original directory name
            File f = new File(theOutputDir	+ FilenameUtils.removeExtension(theCompressedFile.getName()) + ".xml");
            if (!f.isFile()) {
                // Check the parent directory...
                f = f.getAbsoluteFile();
                File parentDir = new File(f.getParent());
                if (!parentDir.exists()) { // ... and create it if necessary
                    parentDir.mkdirs();
                }
            }

            FileOutputStream fop = new FileOutputStream(f);
            long xmlSize = outStream.toByteArray().length;

            if (zipUncompressedSize == xmlSize) {
                fop.write(outStream.toByteArray());
                fop.flush();
            }
            fop.close();


            // check the written file size to the computed file size
            // If they differ, delete the xml file and notify.

            long xmlFileSize = xmlFile.length();
            if (xmlFileSize != zipUncompressedSize) {
                System.out.println("Files do not match, deleting xml file "	+ theOutputDir+ FilenameUtils.removeExtension(theCompressedFile.getName()) + ".xml");
                FileUtils.deleteQuietly(xmlFile);
            }

            //always collect meta data


        } finally {
            if (dec != null) {
                dec.close();
            }
            long endTime = System.currentTimeMillis();

        }
    }

//
//    public void cleanAndWriteMetaData(String metadataOutputDir){
//        duplicatePatientChecker.cleanandWriteMetaDatas(metadataOutputDir);
//    }
//
//
//    /**
//     * Assumptions for duplicate record checks.
//     * 1) This thread is only dealing with a single member's patients
//     * 2) The output directory will not change for each record
//     * 3) We will keep a running list of all patients meta data in memory and reference it for duplicate checking
//     * 4) If we find duplicates:
//     * 	4.a) We will move the first duplicate from the output directory to the duplicates directory ie (outputdir/duplicates/memberId/files)
//     *  4.b) will will unzip the second duplicate into the duplicates ie (outputdir/duplicates/memberId/files)
//     */
//
//    /**
//     *
//     * @param xmlOutputFile
//     * @param xmldata
//     * @param metadataOutputDir
//     */
//
//    private void writeMetaData(MetaData metadata, String metadataOutputDir) throws Exception{
//
//        try {
//
//            duplicatePatientChecker.addRecord(metadata);
//        } catch (Exception e) {
//            // Do nothing
//            e.printStackTrace();
//        }
//
//        //check if metaDataOutputDir exists if not created it
//
//        File metaDataFile = new File(metadataOutputDir+metadata.getMember_id()+".txt");
//        File parentDir = new File(metaDataFile.getAbsoluteFile().getParent());
//        if (!parentDir.exists()) { // ... and create it if necessary
//            parentDir.mkdirs();
//        }
//        //check if the member_id file exists if not created, if it does append to the files
//        //if(metaDataFile.exists() && !metaDataFile.isDirectory()){
//        try {
//            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(metadataOutputDir+metadata.getMember_id()+".txt", true)));
//            out.println(metadata.printMetaData());
//            out.close();
//        } catch (IOException e) {
//            //exception handling left as an exercise for the reader
//            e.printStackTrace();
//        }
//        //}
//
//    }

}
