package com.q.winzipeas.xml.metadata;

import java.io.*;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


/**
 * echo $xmlLoc"|"$(stat -c %s $xmlLoc)"|"$(xml sel -N "h=urn:hl7-org:v3"  -t -v "h:ClinicalDocument/h:recordTarget/h:patientRole/h:id[3]/@extension" $xmlLoc)"|"$(xml sel -N "h=urn:hl7-org:v3" -t -v "h:ClinicalDocument/h:id/@extension[1]" $xmlLoc)"|"$(date +"%m-%d-%Y")>>$2$dirName.txt
 * @author q798470
 *
 */
public class XmlMetaData implements Serializable{


    /**
     *
     * @param member_id
     * @param xmloutPutPath
     * @param pastXMLMetaDatapath
     * @param theXML
     * @return
     * @throws Exception
     */
    public static MetaData getMetaData(String member_id, String xmloutPutPath,String pastXMLMetaDatapath, byte[] theXML)throws Exception{

        MetaData newMetaData = getxmlMetadata(theXML);
        newMetaData.setMember_id(member_id);
        newMetaData.setXmloutPutPath(xmloutPutPath);
		/*if(pastXMLMetaDatapath.trim().length() == 0){
			pastXMLMetaDatapath = null;
		}*/
        if(pastXMLMetaDatapath != null){
            checkPastMetaData(member_id, pastXMLMetaDatapath, newMetaData);
        }else{
            newMetaData.setIsNewXML(true);
        }
        return newMetaData;

    }

    /**
     * Get the past meta data for the supplied member
     * Assumption is the file we are looking for is named as <memberid>.txt
     * @param member_id
     * @param pastXMLMetaDatapath
     * @param newMetaData
     * @throws Exception
     */
    private static void checkPastMetaData(String member_id,String pastXMLMetaDatapath,MetaData newMetaData){
        File pastMemberMetadataFile = getPastMemberMetadataFile(member_id,pastXMLMetaDatapath);
        boolean found=false;
        if(pastMemberMetadataFile != null){
            try{
                BufferedReader br=new BufferedReader(new FileReader(pastMemberMetadataFile));
                String strLine = null;

                while(( strLine = br.readLine())!= null){
                    String[] attributes =strLine.split("\\|");
                    if(newMetaData.getPatient_id().equals(attributes[4])){
                        if(!newMetaData.getCheck_sum().equals(attributes[2])){
                            newMetaData.delta=true;
                            found=true;
                            break;
                        }
                        found=true;
                        break;
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
                //do nothing we were not able to find old meta-data
            }
            if(!found){
                newMetaData.newXML=true;
            }
        }
    }


    /**
     * Only return the past MetaData file if it exists.
     * @param member_id
     * @param pastXMLMetaDatapath
     * @return
     */
    private static File getPastMemberMetadataFile(String member_id, String pastXMLMetaDatapath){
        File dir = new File(pastXMLMetaDatapath);
        if(dir.isDirectory()){
            File[] files = dir.listFiles();
            for (File file : files) {
                if(file.getName().startsWith(member_id)){
                    return file;
                }
            }
        }
        return null;
    }

    /**
     *
     * @param theXml
     * @return
     * @throws Exception
     */
    public static MetaData getxmlMetadata(byte[] theXml)throws Exception{
        MetaData md = new MetaData();

        final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(theXml));

        final XPathExpression patient_id_expression = XPathFactory.newInstance().newXPath().compile("/ClinicalDocument/recordTarget/patientRole/id[3]/@extension");

        final XPathExpression document_id_expression = XPathFactory.newInstance().newXPath().compile("/ClinicalDocument/id/@extension[1]");

        Object patientId_result = patient_id_expression.evaluate(document, XPathConstants.NODESET);
        NodeList patientnodes = (NodeList) patientId_result;
        md.setPatient_id(patientnodes.item(0).getNodeValue());
        Object documentid_result = document_id_expression.evaluate(document, XPathConstants.NODESET);
        NodeList documentnodes = (NodeList) documentid_result;
        md.setDocument_id(documentnodes.item(0).getNodeValue());

        Checksum xmlCheckSum = new CRC32();
        xmlCheckSum.update(theXml, 0, theXml.length);
        String theCheckSum=""+xmlCheckSum.getValue();
        md.setCheck_sum(theCheckSum);
        md.setXmlByteLength((""+theXml.length).trim());

        return md;
    }




}

