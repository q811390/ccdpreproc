package com.q.winzipeas.xml.metadata;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

public class MetaData implements Serializable{

    private String patient_id;
    private String document_id;
    private String check_sum;
    private String member_id;
    private String xmlByteLength;
    protected boolean delta=false;
    protected boolean newXML=false;
    protected boolean duplicate=false;
    protected String xmloutPutPath;
    private int errorCode;
    private String errorMsg;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isDuplicate() {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }
    public String getXmloutPutPath() {
        return xmloutPutPath;
    }

    public void setXmloutPutPath(String xmloutPutPath) {
        this.xmloutPutPath = xmloutPutPath;
    }

    public String getXmlByteLength() {
        return xmlByteLength;
    }

    public void setXmlByteLength(String xmlByteLength) {
        this.xmlByteLength = xmlByteLength;
    }

    public boolean isDelta() {
        return delta;
    }

    public boolean isNewXML() {
        return newXML;
    }

    public void setIsNewXML(boolean isnew){
        newXML=isnew;
    }

    public String getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }
    public String getDocument_id() {
        return document_id;
    }
    public void setDocument_id(String document_id) {
        this.document_id = document_id;
    }
    public String getCheck_sum() {
        return check_sum;
    }
    public void setCheck_sum(String check_sum) {
        this.check_sum = check_sum;
    }
    public String getMember_id() {
        return member_id;
    }
    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    /**
     * echo full_xml_file_path"|"xml_file_size_in_bytes"|"$(xml sel -N "h=urn:hl7-org:v3"  -t -v "h:ClinicalDocument/h:recordTarget/h:patientRole/h:id[3]/@extension" $xmlLoc)"|"$(xml sel -N "h=urn:hl7-org:v3" -t -v "h:ClinicalDocument/h:id/@extension[1]" $xmlLoc)"|"$(date +"%m-%d-%Y")>>$2$dirName.txt
     * returns a String containing a delimited list of the following:
     * 1) the full path to the xml file
     * 2) the xml file size in bytes
     * 3) the patient id
     * 4) the document_id
     * 5) the data of processing
     * @param theXml
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws XPathExpressionException
     */

    public String printMetaData() {

        List<String> results = new ArrayList<String>();
        results.add(getMember_id());
        File ff = new File(xmloutPutPath);
        results.add(ff.getName());
        results.add(getCheck_sum());
        results.add(getXmlByteLength());
        results.add(getPatient_id());
        results.add(getDocument_id());
        results.add((""+(isDelta()? 1: 0)).trim());
        results.add((""+(isNewXML()?1:0)).trim());
        results.add((""+(isDuplicate()?1:0)).trim());
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        results.add(sdf.format(new Date()));
        StringBuffer ret = new StringBuffer("");
        for (int i = 0; i < results.size(); i++) {
            ret.append(results.get(i));
            if (i < results.size() - 1) {
                ret.append('|');
            }
        }
        //System.out.println(ret.toString());
        return ret.toString();

    }

}
