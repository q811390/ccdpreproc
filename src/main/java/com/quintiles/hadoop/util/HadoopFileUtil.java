package com.quintiles.hadoop.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.tools.DistCp;
import org.apache.hadoop.tools.DistCpOptions;
import org.apache.hadoop.util.Progressable;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cloudera on 6/10/15.
 */
public class HadoopFileUtil {

    public static DistCpOptions moveFiles(String source, String dest,int NUM_SPLITS) throws Exception {
        Path targetPath=new Path(dest );
        Path sourcePath=new Path(source );
        List<Path> sourceList=new ArrayList<Path>();
        sourceList.add(sourcePath);
        DistCpOptions options=new DistCpOptions(sourceList,targetPath);
        options.setMaxMaps(NUM_SPLITS);

       // Configuration conf = destCluster.getHadoopConf();
        DistCp distCp=new DistCp(new Configuration(),options);
        try {
            distCp.execute();
        }
        catch (  Exception e) {
            //LOG.error("Exception encountered ",e);
            throw e;
        }
        return options;
    }


    public static void writeHadoopFile(Map<String,String> map, String path, String uri) throws Exception {


        System.out.println("  map :::  "+map);
        Configuration configuration = new Configuration();
        FileSystem hdfs = FileSystem.get( new URI( uri), configuration );
        Path file = new Path(uri+path+"/SUCCESS");
        if ( hdfs.exists( file )) { hdfs.delete( file, true ); }
        OutputStream os = hdfs.create( file,
                new Progressable() {
                    public void progress() {
                        //out.println("...bytes written: [ "+bytesWritten+" ]");
                    } });
        BufferedWriter br = new BufferedWriter( new OutputStreamWriter( os, "UTF-8" ) );
        for (String s : map.keySet()) {
            br.write(s + "=" + map.get(s));
            br.newLine();
        }

        br.close();
        hdfs.close();
    }

    public static void main(String[] args) {
        try {
            moveFiles("webhdfs://usadc-shdsxt04:50070/user/webadm/result1/*", "webhdfs://usadc-shdcxp22:50070/user/q811390/", 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}