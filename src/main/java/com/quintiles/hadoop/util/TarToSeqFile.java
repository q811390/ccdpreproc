/* TarToSeqFile.java - Convert tar files into Hadoop SequenceFiles.
 *
 * Copyright (C) 2008 Stuart Sierra
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You
 * may obtain a copy of the License at
 * http:www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.quintiles.hadoop.util;

/* From ant.jar, http://ant.apache.org/ */


/* From hadoop-*-core.jar, http://hadoop.apache.org/
 * Developed with Hadoop 0.16.3. */
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;


/** Utility to convert tar files into Hadoop SequenceFiles.  The tar
 * files may be compressed with GZip or BZip2.  The output
 * SequenceFile will be stored with BLOCK compression.  Each key (a
 * Text) in the SequenceFile is the name of the file in the tar
 * archive, and its value (a BytesWritable) is the contents of the
 * file.
 *
 * <p>This class can be run at the command line; run without
 * arguments to get usage instructions.
 *
 * @author Stuart Sierra (mail@stuartsierra.com)
 * @see <a href="http://hadoop.apache.org/core/docs/r0.16.3/api/org/apache/hadoop/io/SequenceFile.html">SequenceFile</a>
 * @see <a href="http://hadoop.apache.org/core/docs/r0.16.3/api/org/apache/hadoop/io/Text.html">Text</a>
 * @see <a href="http://hadoop.apache.org/core/docs/r0.16.3/api/org/apache/hadoop/io/BytesWritable.html">BytesWritable</a>
 */
public class TarToSeqFile {

    private File inputFile;
    private File outputFile;
    private LocalSetup setup;

    /** Sets up Configuration and LocalFileSystem instances for
     * Hadoop.  Throws Exception if they fail.  Does not load any
     * Hadoop XML configuration files, just sets the minimum
     * configuration necessary to use the local file system.
     */
    public TarToSeqFile() throws Exception {
        setup = new LocalSetup();
    }

    /** Sets the input tar file. */
    public void setInput(File inputFile) {
        this.inputFile = inputFile;
    }

    /** Sets the output SequenceFile. */
    public void setOutput(File outputFile) {
        this.outputFile = outputFile;
    }

    /** Performs the conversion. */
    public void execute() throws Exception {

        List<String> files = listFilesForFolder(inputFile);
        SequenceFile.Writer output = null;
        try {
            output = openOutputFile();
            for(String filename  : files ) {

                byte[] data = getBytes(filename);
                
                Text key = new Text(Paths.get(filename).getFileName().toString());
                BytesWritable value = new BytesWritable(data);
                output.append(key, value);
            }
        } finally {
            if (output != null) { output.close(); }
        }
    }

    /**
     * Return the zip files in the directory recursively.
     * @param folder
     * @return
     */
    public List<String> listFilesForFolder(final File folder) {
        if(!folder.isDirectory()){
            System.out.println(" this is not a valid directory "+folder.getAbsolutePath());
        }
        List<String> files = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else if(fileEntry.isFile() && fileEntry.getName().endsWith(".zip")){
                files.add(fileEntry.getAbsolutePath().toString());//System.out.println(fileEntry.getName());
            }
        }
        return files;
    }

    private SequenceFile.Writer openOutputFile() throws Exception {
        Path outputPath = new Path(outputFile.getAbsolutePath());
        return SequenceFile.createWriter(setup.getLocalFileSystem(), setup.getConf(),
                                         outputPath,
                                         Text.class, BytesWritable.class,
                                         SequenceFile.CompressionType.BLOCK);
    }

    /** Reads all bytes from the current entry in the tar file and
     * returns them as a byte array.
     *
     *
     */
    private static byte[] getBytes(String inputFile) throws Exception {


        byte[] bytes = Files.readAllBytes(Paths.get(inputFile));

        return bytes;
    }


    public void createSeqFile(String inputFolder,String seqFilePath) throws Exception {
        setInput(new File(inputFolder));
        setOutput(new File(seqFilePath));
        execute();
    }


    /** Runs the converter at the command line. */
    public static void main(String[] args) {
        if (args.length != 2) {
            exitWithHelp();
        }

        try {
            TarToSeqFile me = new TarToSeqFile();
            me.createSeqFile(args[0],args[1]);
        } catch (Exception e) {
            e.printStackTrace();
            exitWithHelp();
        }


    }

    public static void exitWithHelp() {
        System.err.println("Usage: java org.altlaw.hadoop.TarToSeqFile <tarfile> <output>\n\n" +
                           "Folder with zip files and sequence file name");
        System.exit(1);
    }
}
