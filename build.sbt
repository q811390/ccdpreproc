import sbt.Keys._
import sbt._

name := "CCDPreProc"

version := "1.0"

scalaVersion := "2.10.4"

scalacOptions += "-target:jvm-1.7"

javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.1" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.3.1" % "provided"

libraryDependencies += "com.databricks" %% "spark-avro" % "1.0.0"

libraryDependencies += "com.stackmob" %% "newman" % "1.3.5"

libraryDependencies += "joda-time" % "joda-time" % "2.7"

libraryDependencies += "de.idyl" % "winzipaes" % "1.0.1"

libraryDependencies += "org.apache.hadoop" % "hadoop-distcp" % "2.3.0"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.2.4" % "test"

resolvers += "justwrote" at "http://repo.justwrote.it/releases/"

resolvers += "Restlet Repository" at "http://maven.restlet.org"

libraryDependencies += "it.justwrote" %% "scala-faker" % "0.3"
//libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.2.1" % "test"

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) => {
  case PathList("javax", "pom.properties", xs@_*) => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".class" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".RSA" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith "mailcap" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".xml" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".dtd" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".default" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".xsd" => MergeStrategy.first
  case "pom.properties" => MergeStrategy.first
  case "unwanted.txt" => MergeStrategy.discard
  case x => old(x)
}
}

test in assembly := {}

assemblyJarName in assembly := "ccd_preprocessor.jar"


